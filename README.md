# EZ Travel Backend Challenge

Create a simple backend service able to accept RESTful requests. This service must receive a city name as a parameter and return the following information:

- Location informed
- Date (request date in format dd/mm/yyyy)
- Current time (request time hh:mm:ss)
- Temperature (celsius)
- Max temperature (celsius)
- Min temperature (celsius)
- Sunrise time (hh:mm)
- Sunset time (hh:mm)
- Suggestion (see business rules)

## Business rules for suggestion

- If temperature (celsius) is above 30 degrees and current time is before sunset, suggestion must return 'Don't forget the sun lotion'
- If temperature (celsius) is above 30 degrees and current time is after sunset, suggestion must return 'Cool down and relax'
- In case temperature is between 20 and 30 degrees, suggestion must return 'Don't panic, you will be fine'
- If it's a bit chilly (between 10 and 19 degrees), suggestion must return 'Take your coat with you'
- Otherwise, if it's freezing and current time is before sunset, suggestion must be: 'Just a coat will not be enough'

## Expected results

In case of success

```shell
❯ curl --location --request GET '<your-solution-url>?city=fortaleza'
```

response: 200 OK

```json
{
  "location": "fortaleza",
  "date": "16/10/2020",
  "current_time": "19:17:49",
  "temp": 26.86,
  "temp_min": 26.67,
  "temp_max": 27,
  "sunrise": "05:13",
  "sunset": "17:25",
  "suggestion": "Don't panic, you will be fine"
}
```

If the city are not found

```shell
❯ curl --location --request GET '<your-solution-url>?city=ksdjafjsa'
```

response: 400 Not found

```json
{
  "error": "404",
  "message": "city not found"
}
```

## Hints

You can use OpenWeatherMaps API (https://openweathermap.org) to fetch weather and
temperature data.

For sunrise and sunset times you could use IpGeolocation (https://api.ipgeolocation.io). You can use either `Getting Astronomical Information for an Address` or `Getting Astronomical Information for Location Coordinates` aproach. For the last one, you can use the lat, lang information returned from the OpenWeatherMaps API.

- [OpenWeatherMaps API](https://home.openweathermap.org/users/sign_up) (You can use this API Key: 31947444a970388d50293db53ddedc9f)
- [IpGeolocation API](https://ipgeolocation.io/documentation/astronomy-api.html) (You can use this Client Id: 743a23e8a7b543ba8a5efeee842aa445)

### Sample external API calls

Getting weather info from `OpenWeatherMaps API`

```shell
❯ curl --location --request GET 'https://api.openweathermap.org/data/2.5/weather?q=fortaleza&appid=31947444a970388d50293db53ddedc9f&units=metric&lang=pt_br'
```

response

```json
{
  "coord": {
    "lon": -38.52,
    "lat": -3.72
  },
  "weather": [
    {
      "id": 802,
      "main": "Clouds",
      "description": "nuvens dispersas",
      "icon": "03n"
    }
  ],
  "base": "stations",
  "main": {
    "temp": 26.86,
    "feels_like": 28.25,
    "temp_min": 26.67,
    "temp_max": 27,
    "pressure": 1012,
    "humidity": 74
  },
  "visibility": 10000,
  "wind": {
    "speed": 4.6,
    "deg": 70
  },
  "clouds": {
    "all": 33
  },
  "dt": 1602885856,
  "sys": {
    "type": 1,
    "id": 8363,
    "country": "BR",
    "sunrise": 1602836017,
    "sunset": 1602879908
  },
  "timezone": -10800,
  "id": 6320062,
  "name": "Fortaleza",
  "cod": 200
}
```

Getting additional info from `IpGeolocation API`

```shell
❯ curl --location --request GET 'https://api.ipgeolocation.io/astronomy?apiKey=743a23e8a7b543ba8a5efeee842aa445&location=fortaleza'
```

response

```json
{
  "location": {
    "location": "fortaleza",
    "latitude": -3.7304512,
    "longitude": -38.5217989
  },
  "date": "2020-10-16",
  "current_time": "19:17:49.929",
  "sunrise": "05:13",
  "sunset": "17:25",
  "sun_status": "-",
  "solar_noon": "11:19",
  "day_length": "12:12",
  "sun_altitude": -28.40277183872628,
  "sun_distance": 149125136.32560483,
  "sun_azimuth": 257.30835031851666,
  "moonrise": "05:00",
  "moonset": "17:28",
  "moon_status": "-",
  "moon_altitude": -25.902978388147435,
  "moon_distance": 364143.3932140178,
  "moon_azimuth": 261.518928521363,
  "moon_parallactic_angle": 91.4837806299539
}
```

## Non functional requirements

The service must be prepared to be fault tolerant, responsive and resilient.

You must build your solution using Node.js. Use whatever framework and tools you feel comfortable with. In Readme.md is desirable that you briefly elaborate on your solution, architecture details, choice of patterns and frameworks.

Also, make it easy to deploy/run your service(s) locally.

Once you have done, share your code with us on your github.

May the force be with you! :-)